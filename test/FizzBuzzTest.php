<?php

require_once "vendor/autoload.php";

use App\FizzBuzz;
use PHPUnit\Framework\TestCase;

class FizzBuzzTest extends TestCase
{
    /** @var  FizzBuzz */
    private $fizzBuzz;

    public function setUp()
    {
        $this->fizzBuzz = new FizzBuzz();
    }

    /**
     * @test If test range is not between 1 and 100, exception is expected.
     * @expectedException InvalidArgumentException
     */
    public function testIfRangeIsNotBetween1And100ShouldThrowException()
    {
        $this->fizzBuzz->calculate(0, -5);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function testIfRangeIsOverpassedShouldThrowException()
    {
        $this->fizzBuzz->calculate(1, 101);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function testIfRangeIsUnderpassedShouldThrowException()
    {
        $this->fizzBuzz->calculate(0,1);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function testIfFirstArgumentIsNegativeShouldThrowException()
    {
        $this->fizzBuzz->calculate(-11, 101);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function testIfFirstNumberIsGreaterThanTheSecondShouldThrowException()
    {
        $this->fizzBuzz->calculate(100, 99);
    }

    /**
     * @test
     */
    public function testIfRangeValidBetween1And3ShouldReturnAFizz()
    {
        $expectedResult = '1,2,Fizz';
        $result = $this->fizzBuzz->calculate(1, 3);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @test
     */
    public function testIfRangeValidBetween1And5ShouldReturnAFizzAndABuzz()
    {
        $expectedResult = '1,2,Fizz,4,Buzz';
        $result = $this->fizzBuzz->calculate(1, 5);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @test
     */
    public function testIfRangeValidWithA15ShouldReturnCorrectFizzBuzz()
    {
        $expectedResult = '14,FizzBuzz,16';
        $result = $this->fizzBuzz->calculate(14, 16);

        $this->assertEquals($expectedResult, $result);
    }


    /**
     * @test
     */
    public function testIfRangeValidFrom1To16ShouldReturnCorrectFizzBuzz()
    {
        $expectedResult = '1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,FizzBuzz,16';
        $result = $this->fizzBuzz->calculate(1, 16);

        $this->assertEquals($expectedResult, $result);
    }


    /**
     * @test
     */
    public function testIfSingleNumberRangeShouldReturnCorrectResult()
    {
        $expectedResult = '1';
        $result = $this->fizzBuzz->calculate(1, 1);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @test
     */
    public function testIfSingleNumberRangeWithValue15ShouldReturnCorrectResult()
    {
        $expectedResult = 'FizzBuzz';
        $result = $this->fizzBuzz->calculate(15, 15);

        $this->assertEquals($expectedResult, $result);
    }

}