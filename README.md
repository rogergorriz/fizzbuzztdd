# README #

### What is this repository for? ###

This is a quick example of how TDD works in the classical FizzBuzz problem.

To run the project, you have to clone it and execute:

`composer install`

If you want to run the tests:

`phpunit test/`

Note that you will have to run it on PHP7 or higher. That's because the code uses some features of PHP7 (type hinting, return type declaration, short array syntax...)