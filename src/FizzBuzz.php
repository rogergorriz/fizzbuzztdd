<?php

namespace App;

use InvalidArgumentException;

class FizzBuzz
{
    const MAX_NUMBER_IN_RANGE = 100;

    const MIN_NUMBER_IN_RANGE = 1;

    public function calculate(int $number1, int $number2)
    {
        $result = [];

        if (!$this->isRangeValid($number1, $number2)) {
            throw new InvalidArgumentException();
        }

        for ($i = $number1; $i <= $number2; $i++) {

            if ($i % 15 == 0) {
                $result[] = 'FizzBuzz';
            } elseif ($i % 3 == 0) {
                $result[] = 'Fizz';
            } elseif ($i % 5 == 0) {
                $result[] = 'Buzz';
            } else {
                $result[] = $i;
            }
        }

        return implode(',', $result);
    }


    private function isRangeValid(int $number1, int $number2): bool
    {
        return !$this->anyNumberUnderpassLimit($number1, $number2) && !$this->anyNumberOverpassLimit($number1,
                $number2) && !$this->firstNumberIsGreaterThanSecondNumber($number1, $number2);
    }

    /**
     * @param int $number1
     * @param int $number2
     * @return bool
     */
    private function anyNumberUnderpassLimit(int $number1, int $number2): bool
    {
        return $number1 < self::MIN_NUMBER_IN_RANGE || $number2 < self::MIN_NUMBER_IN_RANGE;
    }

    private function anyNumberOverpassLimit(int $number1, int $number2): bool
    {
        return $number1 > self::MAX_NUMBER_IN_RANGE || $number2 > self::MAX_NUMBER_IN_RANGE;
    }

    private function firstNumberIsGreaterThanSecondNumber(int $number1, int $number2): bool
    {
        return $number1 > $number2;
    }
}